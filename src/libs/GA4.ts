import * as path from 'path';
import * as fs from 'fs';
import Logger from './Logger';
import { BetaAnalyticsDataClient } from '@google-analytics/data';
import { IOCServiceName } from '../ioc/IocServiceName';
import { Service, Container } from 'typedi';
import { IGA4Config, IConfig } from '../configs';

export interface IGA4 {
    getInstance(): BetaAnalyticsDataClient;
    getGA4Config(): IGA4Config;
}

// Google Analytics Data API
@Service(IOCServiceName.LIB_GA4)
export class GA4 {
    private instance: BetaAnalyticsDataClient;
    private GA4Config: IGA4Config;

    constructor() {
        const credentialsJsonPath = '../configs/env/ga4.json';
        const url = path.join(__dirname, credentialsJsonPath);

        if (fs.existsSync(url)) {
            this.instance = new BetaAnalyticsDataClient({
                keyFilename: url
            });
            Logger.info('Init Google Analytics Data Client (GA4)');
        } else {
            Logger.warn(`Firebase config file - ${credentialsJsonPath} does not exist, skip`);
        }

        const config: IConfig = Container.get<IConfig>(IOCServiceName.SYSTEM_CONFIG);
        this.GA4Config = config.getGA4Config();
    }

    public getInstance(): BetaAnalyticsDataClient {
        return this.instance;
    }

    public getGA4Config(): IGA4Config {
        return this.GA4Config;
    }
}

export default GA4;
