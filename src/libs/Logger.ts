import * as pino from "pino";
import * as trace from "stack-trace";
import { Service } from "typedi";
import { IOCServiceName } from "../ioc/IocServiceName";
export interface ILogger {
    error(message: string, meta?: any): void;
    warn(message: string, meta?: any): void;
    info(message: string, meta?: any): void;
    debug(message: string, meta?: any): void;
}
@Service(IOCServiceName.LIB_LOGGER)
class Logger implements ILogger {
    private processLogger: pino.Logger;
    private transportLogger: pino.Logger;

    constructor() {
        this.processLogger = pino({
            name: "process",
            prettyPrint: true,
            enabled: process.env.NO_LOG !== "true",
            level: process.env.NODE_ENV === "development" ? "debug" : "info",
        });

        this.transportLogger = pino({
            name: "transport",
            enabled: process.env.NO_LOG !== "true",
            level: process.env.NODE_ENV === "development" ? "debug" : "info",
        });
    }

    public getTransportLogger(): ILogger {
        return this.transportLogger;
    }

    public error(message: string, meta?: any, ...args: any[]): void {
        if (meta != null) {
            if (meta instanceof Error && meta.stack != null) {
                this.processLogger.error({ stack: trace.parse(meta) }, message, ...args);
            } else {
                this.processLogger.error(meta, message, ...args);
            }
        } else {
            this.processLogger.error(message, ...args);
        }
    }

    public warn(message: string, meta?: any, ...args: any[]): void {
        if (meta != null) {
            this.processLogger.warn(meta, message, ...args);
        } else {
            this.processLogger.warn(message, ...args);
        }
    }

    public info(message: string, meta?: any, ...args: any[]): void {
        if (meta != null) {
            this.processLogger.info(meta, message, ...args);
        } else {
            this.processLogger.info(message, ...args);
        }
    }

    public debug(message: string, meta?: any, ...args: any[]): void {
        if (meta != null) {
            this.processLogger.debug(meta, message, ...args);
        } else {
            this.processLogger.debug(message);
        }
    }
}
export default new Logger();
