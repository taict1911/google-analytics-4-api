import { Service } from 'typedi';
import { IConfig } from '../../../configs';
import { IOCServiceName } from '../../../ioc/IocServiceName';
import * as ClickHouseConnection from '@apla/clickhouse';
import Logger from "../../../libs/Logger";

export interface IDatabaseConnection {
    getConnection(): ClickHouseConnection;
    createConnection(config: IConfig): Promise<ClickHouseConnection>;
}

@Service(IOCServiceName.DATABASE_CLICKHOUSE_CONNECTION)
export class DataBaseConnection implements IDatabaseConnection {
    private instance: ClickHouseConnection;

    getConnection(): ClickHouseConnection {
        return this.instance;
    }

    async createConnection(config: IConfig): Promise<ClickHouseConnection> {
        const clickHouseConfig = config.getClickHouseConfig();
        this.instance = new ClickHouseConnection({
            host: clickHouseConfig.host,
            port: clickHouseConfig.port,
            user: clickHouseConfig.user,
            password: clickHouseConfig.password,
            dataObjects: true
        });

        let testConnecion = await this.instance.pinging().catch((err) => {
            Logger.error(`ClickHouse connection failed": ${err.message}`, err);
        });
        if (testConnecion) {
            Logger.info("ClickHouse connection is OK");
        }
        return this.instance;
    }
}
