export const BASE_TABLE_SCHEMA = {
    TABLE_NAME: 'base_tables',
    FIELDS: {
        ID: 'id',
        IS_ENABLE: 'is_enable',
        IS_DELETED: 'is_deleted',
        CREATED_DATE: 'created_date',
        UPDATED_DATE: 'updated_date',
    },
};
