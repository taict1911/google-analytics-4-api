export const ACTIVE_USER_DAILY_TABLE_SCHEMA = {
    TABLE_NAME: "active_user_daily",
    FIELDS: {
        ID: "id",
        IS_ENABLE: "is_enable",
        IS_DELETED: "is_deleted",
        CREATED_DATE: "created_date",
        UPDATED_DATE: "updated_date",
        ACTIVE_DATE: "active_date",
        ACTIVE_1_DAY_USERS: "active_1_day_users",
        ACTIVE_7_DAY_USERS: "active_7_day_users",
        ACTIVE_28_DAY_USERS: "active_28_day_users"
    }
};

export const ACTIVE_USER_REALTIME_TABLE_SCHEMA = {
    TABLE_NAME: "active_user_realtime",
    FIELDS: {
        ID: "id",
        IS_ENABLE: "is_enable",
        IS_DELETED: "is_deleted",
        CREATED_DATE: "created_date",
        UPDATED_DATE: "updated_date",
        ACTIVE_DATE: "active_date",
        USERS_PER_MINUTE: "users_per_minute",
        TOP_COUNTRIES: "top_countries",
        TOTAL_ACTIVE_USER: "total_active_user"
    }
};
