import * as ClickHouseConnection from '@apla/clickhouse';
import { Container, Inject } from 'typedi';
import { IDatabaseConnection } from '../connection';
import { IOCServiceName } from '../../../ioc/IocServiceName';
import { ILogger } from '../../../libs/Logger';
import { IClickHouseConfig, IConfig } from '../../../configs';

const Logger = Container.get<ILogger>(IOCServiceName.LIB_LOGGER);

class BaseRepository {
    private clickHouseConfig: IClickHouseConfig;
    private connection: ClickHouseConnection;
    @Inject(IOCServiceName.LIB_LOGGER)
    private readonly Logger: ILogger;
    constructor() {
        const databaseConnection = Container.get<IDatabaseConnection>(IOCServiceName.DATABASE_CLICKHOUSE_CONNECTION);
        this.connection = databaseConnection.getConnection();
        const config: IConfig = Container.get<IConfig>(IOCServiceName.SYSTEM_CONFIG);
        this.clickHouseConfig = config.getClickHouseConfig();
    }

    public getGA4Config(): IClickHouseConfig {
        return this.clickHouseConfig;
    }

    public async raw(query: string): Promise<any> {
        const result = await this.connection.querying(query)
            .catch((err) => {
                Logger.error(err.message, err);
            });
        return result;
    }

    public async get(query: string): Promise<any> {
        let rows: string[] = [];
        const stream = this.connection.query(query, {}, (err, data) => {});
        return new Promise(function(resolve, reject) {
            stream.on('data', (row) => {
                rows.push(row);
            });
            stream.on('end', () => {
                resolve(rows);
            });
        });
    }
}

export default BaseRepository;
