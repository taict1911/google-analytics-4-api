import { IActiveUserDailyRepository } from '../../../app/interfaces/repository';
import { Service } from 'typedi';
import { ActiveUserDailyModel } from '../../../app/models';
import { IOCServiceName } from '../../../ioc/IocServiceName';
import BaseRepository from './BaseRepository';
import { ACTIVE_USER_DAILY_TABLE_SCHEMA } from '../schemas';
import { MOMENT_DATE_FORMAT, } from '../../../app/constants';
import * as SqlString from 'sqlstring';

@Service(IOCServiceName.ACTIVE_USER_DAILY_REPOSITORY)
export class ActiveUserDailyRepository extends BaseRepository implements IActiveUserDailyRepository {
    public async add(model: ActiveUserDailyModel): Promise<any> {
        const clickHouseConfig = this.getGA4Config();

        const dateString = model.activeDate.format(MOMENT_DATE_FORMAT.YYYY_MM_DD_H_m_s);
        const formatQuery = SqlString.format(`
            INSERT INTO ${clickHouseConfig.database}.${ACTIVE_USER_DAILY_TABLE_SCHEMA.TABLE_NAME} 
            (   ${ACTIVE_USER_DAILY_TABLE_SCHEMA.FIELDS.ACTIVE_DATE}, 
                ${ACTIVE_USER_DAILY_TABLE_SCHEMA.FIELDS.ACTIVE_1_DAY_USERS}, 
                ${ACTIVE_USER_DAILY_TABLE_SCHEMA.FIELDS.ACTIVE_7_DAY_USERS}, 
                ${ACTIVE_USER_DAILY_TABLE_SCHEMA.FIELDS.ACTIVE_28_DAY_USERS}) 
            VALUES
            (?, ?, ?, ?) 
        `, [dateString, model.active1DayUsers, model.active7DayUsers, model.active28DayUsers]);
        return await this.raw(formatQuery)
            .then((result) => {
                return model;
            });
    }
}
