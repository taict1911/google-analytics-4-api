import { IActiveUserRealtimeRepository } from '../../../app/interfaces/repository';
import { Service } from 'typedi';
import { ActiveUserRealtimeModel } from '../../../app/models';
import { IOCServiceName } from '../../../ioc/IocServiceName';
import BaseRepository from './BaseRepository';
import { ACTIVE_USER_REALTIME_TABLE_SCHEMA } from '../schemas';
import { MOMENT_DATE_FORMAT } from '../../../app/constants';
import * as SqlString from 'sqlstring';

@Service(IOCServiceName.ACTIVE_USER_REALTIME_REPOSITORY)
export class ActiveUserRealtimeRepository extends BaseRepository implements IActiveUserRealtimeRepository {
    public async add(model: ActiveUserRealtimeModel): Promise<any> {
        const clickHouseConfig = this.getGA4Config();

        const dateString = model.activeDate.format(MOMENT_DATE_FORMAT.YYYY_MM_DD_H_m_s);
        const usersPerMinuteString = JSON.stringify(model.usersPerMinute);
        const topCountriesString = JSON.stringify(model.topCountries);

        const formatQuery = SqlString.format(`
            INSERT INTO ${clickHouseConfig.database}.${ACTIVE_USER_REALTIME_TABLE_SCHEMA.TABLE_NAME} 
            (   ${ACTIVE_USER_REALTIME_TABLE_SCHEMA.FIELDS.ACTIVE_DATE}, 
                ${ACTIVE_USER_REALTIME_TABLE_SCHEMA.FIELDS.TOTAL_ACTIVE_USER}, 
                ${ACTIVE_USER_REALTIME_TABLE_SCHEMA.FIELDS.USERS_PER_MINUTE}, 
                ${ACTIVE_USER_REALTIME_TABLE_SCHEMA.FIELDS.TOP_COUNTRIES}) 
            VALUES
            (?, ?, ?, ?) 
        `, [dateString, model.totalActiveUser, usersPerMinuteString, topCountriesString]);
        return await this.raw(formatQuery)
            .then((result) => {
                return model;
            });
    }
}
