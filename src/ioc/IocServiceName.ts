export const IOCServiceName = {
    // service
    REPORT_SERVICE: 'report.service',
    GA4_SERVICE: 'google.analytic.api.service',
    IMPORT_DATA_SERVICE: ' Import.data.service',

    // system
    SYSTEM_CONFIG: 'system.config',

    // database
    DATABASE_CLICKHOUSE_CONNECTION: 'database.clickhouse.connection',

    // repository
    ACTIVE_USER_DAILY_REPOSITORY: 'active.user.daily.repository',
    ACTIVE_USER_REALTIME_REPOSITORY: 'active.user.realtime.repository',

    LIB_LOGGER: 'lib.logger',
    LIB_GA4: 'lib.google.analytics.api'
};
