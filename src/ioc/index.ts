import * as fs from 'fs';
import * as path from 'path';
import '../configs';

export class IOC {
    setup(): any {
        // load lib
        (this.loadModule('../libs'));

          // Load interactors module
        (this.loadModule('../app/interactors/ga4'));

        // Load microservice module
        (this.loadModule('../app/microservices'));

        // Load repository module
        (this.loadModule('../infrastructure/clickhouse/connection'));
        (this.loadModule('../infrastructure/clickhouse/repositories'));
    }
    private loadModule(dir: string) {
        const folder = path.join(__dirname, dir);
        fs.readdirSync(folder).forEach((file) => {
            const isJsFile = file.indexOf('.js') >= 0 ? true : false;
            const isTsFile = file.indexOf('.ts') >= 0 ? true : false;
            const isJsMapFile = file.indexOf('.js.map') >= 0 ? true : false;
            if ((isJsFile || isTsFile) && !isJsMapFile) {
                require(`${folder}/${file}`);
            }
        });
    }
}
