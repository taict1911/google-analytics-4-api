import Container from 'typedi';
import { IOC } from './ioc';
import { IDatabaseConnection } from './infrastructure/clickhouse/connection';
import { IImportDataService } from './app/interfaces';
import { IConfig } from './configs';
import { IOCServiceName } from './ioc/IocServiceName';
import Logger from "./libs/Logger";
import 'reflect-metadata';

(async () => {
    await new IOC().setup();

    const systemConfig = Container.get<IConfig>(IOCServiceName.SYSTEM_CONFIG);


    const databaseConnection = Container.get<IDatabaseConnection>(IOCServiceName.DATABASE_CLICKHOUSE_CONNECTION);
    await databaseConnection.createConnection(systemConfig);

    const importDataService = Container.get<IImportDataService>(IOCServiceName.IMPORT_DATA_SERVICE);
    const appConfig = systemConfig.getAppConfig();

    switch (appConfig.mode) {
        case 0:
            Logger.info(`Starting import daily data from GA4 to ClickHouse`);
            await importDataService.importDailyDataFromGA4ToClickHouse();
            Logger.info(`Import daily data from GA4 to ClickHouse successfully`);
            break;
        case 1:
            Logger.info(`Starting import realtime data from GA4 to ClickHouse`);
            await importDataService.importRealtimeDataFromGA4ToClickHouse();
            Logger.info(`Import realtime data from GA4 to ClickHouse successfully`);
            break;
    }
})();
