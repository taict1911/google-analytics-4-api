import Container from 'typedi';
import * as yaml from 'js-yaml';
import * as path from 'path';
import * as fs from 'fs';
import { IOCServiceName } from '../ioc/IocServiceName';
import Logger from "../libs/Logger";

export interface IClickHouseConfig {
    host: string;
    port: number;
    user: string;
    password: string;
    database: string;
}

export interface IGA4Config {
    propertyId: string;
}

export interface IAppConfig {
    mode: number;
}

export interface IConfig {
    port: number;
    getClickHouseConfig(): IClickHouseConfig;
    getGA4Config(): IGA4Config;
    getAppConfig(): IAppConfig;
    init(): void;
}

export class Config implements IConfig {
    public port: number;
    private _clickHouseConfig: IClickHouseConfig;
    private _GA4Config: IGA4Config;
    private _appConfig: IAppConfig;

    constructor() {
        console.log('get contructor configuration .yaml ===>');
    }
    public init(): void {
        Logger.info('init configuration .yaml ======>');
        const url = path.join(__dirname, './env');
        const conf: any = {};
        if (process.env.NODE_ENV == null) {
            process.env.NODE_ENV = 'development';
        }
        try {
            const doc = yaml.safeLoad(fs.readFileSync(`${url}/${process.env.NODE_ENV}.yaml`, 'utf8'));
            if (typeof doc == "object") {
                for (const key of Object.keys(doc)) {
                    const val = doc[key];
                    if (val != null) {
                        conf[key] = val;
                    }
                }
            }
        } catch (err) {
            Logger.error(`Error when loading configuration file ${process.env.NODE_ENV}.yaml, fallback to configuration.yaml`);
            try {
                const doc = yaml.safeLoad(fs.readFileSync(`${url}/configuration.yaml`, 'utf8'));
                if (typeof doc == "object") {
                    for (const key of Object.keys(doc)) {
                        const val = doc[key];
                        if (val != null) {
                            conf[key] = val;
                        }
                    }
                }
            } catch (err) {
                Logger.error(`Error when loading configuration file configuration.yaml, using default value for each module: ${err.message}`);
            }
        }

        this.setClickHouseConfig(conf?.database?.clickhouse);
        this.setGA4Config(conf?.ga4);
        this.setAppConfig(conf?.app);

        this.port = conf?.PORT ? Number(conf?.PORT) : 3000;
    }

    public setClickHouseConfig(config: IClickHouseConfig): void {
        this._clickHouseConfig = config;
    }

    public setGA4Config(config: IGA4Config): void {
        this._GA4Config = config;
    }

    public setAppConfig(config: IAppConfig): void {
        this._appConfig = config;
    }

    public getClickHouseConfig(): IClickHouseConfig {
        if (!this._clickHouseConfig) {
            throw new Error('missing init ClickHouse config ===>');
        }
        return this._clickHouseConfig;
    }

    public getGA4Config(): IGA4Config {
        if (!this._GA4Config) {
            throw new Error('missing init GA4 config ===>');
        }
        return this._GA4Config;
    }

    public getAppConfig(): IAppConfig {
        if (!this._appConfig) {
            throw new Error('missing init App config ===>');
        }
        return this._appConfig;
    }
}

const config = new Config();
config.init();
Container.set(IOCServiceName.SYSTEM_CONFIG, config);
