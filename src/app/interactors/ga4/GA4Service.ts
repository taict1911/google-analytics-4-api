import { Inject, Service } from 'typedi';
import { IOCServiceName } from '../../../ioc/IocServiceName';
import { IGA4Service, IDimensionParam, IMetricParam, IOrderByParam, IDateRangesParam } from '../../interfaces';
import { IGA4 } from '../../../libs/GA4';
import Logger from "../../../libs/Logger";

@Service(IOCServiceName.GA4_SERVICE)
class GA4Service implements IGA4Service {
    @Inject(IOCServiceName.LIB_GA4)
    private readonly ga4: IGA4;

    /**
     *
     * @param startDate startDate must be YYYY-MM-DD, NdaysAgo, yesterday, or today
     * @param endDate endDate must be YYYY-MM-DD, NdaysAgo, yesterday, or today
     * https://developers.google.com/analytics/devguides/reporting/data/v1/rest/v1beta/DateRange
     */
    public async getActiveUserReport(dimensions: IDimensionParam[], metrics: IMetricParam[], dateRanges: IDateRangesParam[], orderBys: IOrderByParam[]): Promise<any> {
        let result: any = null;
        const ga4Client = this.ga4.getInstance();
        const GA4Config = this.ga4.getGA4Config();

        let reportData = await ga4Client.runReport({
            property: `properties/${GA4Config.propertyId}`,
            dateRanges: dateRanges,
            dimensions: dimensions,
            metrics: metrics,
            orderBys: orderBys,
            keepEmptyRows: true
        }).catch((err) => {
            Logger.error(`Error when get report from Google Analytics Data API (GA4): ${err.message}`, err);
        });
        if (reportData && reportData.length > 0) {
            result = reportData[0];
        }
        return result;
    }

    public async getActiveUserRealtimeReport(dimensions: IDimensionParam[], metrics: IMetricParam[], orderBys: IOrderByParam[]): Promise<any> {
        let result: any = null;
        const ga4Client = this.ga4.getInstance();
        const GA4Config = this.ga4.getGA4Config();

        let reportData = await ga4Client.runRealtimeReport({
            property: `properties/${GA4Config.propertyId}`,
            dimensions: dimensions,
            metrics: metrics,
            orderBys: orderBys
        }).catch((err) => {
            throw new Error(`Error when get report from Google Analytics Data API (GA4): ${err.message}`);
        });
        if (reportData && reportData.length > 0) {
            result = reportData[0];
        }
        return result;
    }
}
export default GA4Service;
