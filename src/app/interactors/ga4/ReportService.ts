import { Inject, Service } from 'typedi';
import { IOCServiceName } from '../../../ioc/IocServiceName';
import { IGA4Service, IReportService, IDimensionParam, IMetricParam, IOrderByParam, IDateRangesParam } from '../../interfaces';
import { ActiveUserDailyModel, ActiveUserRealtimeModel } from '../../../app/models';
import { GA4_DATE_RANGE_FORMAT, GA4_REPORT } from '../../constants';

@Service(IOCServiceName.REPORT_SERVICE)
class ReportService implements IReportService {
    @Inject(IOCServiceName.GA4_SERVICE)
    private readonly ga4Service: IGA4Service;

    public async getActiveUserReportOfYesterday(): Promise<ActiveUserDailyModel> {
        const dimensionParams: IDimensionParam[] = [{ name: GA4_REPORT.DIMENSIONS.DATE }];
        const metricParams: IMetricParam[] = [
            {
                name: GA4_REPORT.METRICS.ACTIVE_1_DAY_USERS
            },
            {
                name: GA4_REPORT.METRICS.ACTIVE_7_DAY_USERS
            },
            {
                name: GA4_REPORT.METRICS.ACTIVE_28_DAY_USERS
            }
        ];
        const dateRangesParams: IDateRangesParam[] = [
            {
                startDate: GA4_DATE_RANGE_FORMAT.YESTERDAY,
                endDate: GA4_DATE_RANGE_FORMAT.YESTERDAY
            }
        ];
        const orderByParams: IOrderByParam[] = [
            {
                desc: false,
                dimension: {
                    dimensionName: GA4_REPORT.DIMENSIONS.DATE
                }
            }
        ];
        let reports: any = await this.ga4Service.getActiveUserReport(dimensionParams, metricParams, dateRangesParams, orderByParams);
        let model: ActiveUserDailyModel = new ActiveUserDailyModel();
        model = ActiveUserDailyModel.fromReport(reports);

        return model;
    }

    public async getActiveUserRealtimeReport(): Promise<ActiveUserRealtimeModel> {
        const data = await Promise.all([
            this.getActiveUserPerMinute(),
            this.getActiveUserByCountry(),
            this.getTotalActiveUser()
        ]);

        let model: ActiveUserRealtimeModel = new ActiveUserRealtimeModel();
        model = ActiveUserRealtimeModel.fromReport(data[0], data[1], data[2]);
        return model;
    }

    public async getActiveUserPerMinute(): Promise<any> {
        const dimensionParams: IDimensionParam[] = [{ name: GA4_REPORT.DIMENSIONS.MINUTES_AGO }];
        const metricParams: IMetricParam[] = [{ name: GA4_REPORT.METRICS.ACTIVE_USERS }];
        const orderByParams: IOrderByParam[] = [
            {
                desc: false,
                dimension: {
                    dimensionName: GA4_REPORT.DIMENSIONS.MINUTES_AGO
                }
            }];

        return await this.ga4Service.getActiveUserRealtimeReport(dimensionParams, metricParams, orderByParams);
    }

    public async getActiveUserByCountry(): Promise<any> {
        const dimensionParams: IDimensionParam[] = [{ name: GA4_REPORT.DIMENSIONS.COUNTRY }];
        const metricParams: IMetricParam[] = [{ name: GA4_REPORT.METRICS.ACTIVE_USERS }];
        const orderByParams: IOrderByParam[] = [
            {
                desc: false,
                dimension: {
                    dimensionName: GA4_REPORT.DIMENSIONS.COUNTRY
                }
            }];

        return await this.ga4Service.getActiveUserRealtimeReport(dimensionParams, metricParams, orderByParams);
    }

    public async getTotalActiveUser(): Promise<any> {
        const metricParams: IMetricParam[] = [{ name: GA4_REPORT.METRICS.ACTIVE_USERS }];

        return await this.ga4Service.getActiveUserRealtimeReport([], metricParams, []);
    }
}
export default ReportService;
