import { Inject, Service } from 'typedi';
import { IOCServiceName } from '../../ioc/IocServiceName';
import { IReportService } from '../interfaces';
import { IActiveUserDailyRepository, IImportDataService, IActiveUserRealtimeRepository } from '../interfaces';
import { ActiveUserDailyModel, ActiveUserRealtimeModel } from '../models';

@Service(IOCServiceName.IMPORT_DATA_SERVICE)
class ImportDataService implements IImportDataService {
    @Inject(IOCServiceName.REPORT_SERVICE)
    private readonly ReportService: IReportService;

    @Inject(IOCServiceName.ACTIVE_USER_DAILY_REPOSITORY)
    private readonly ActiveUserDailyRepository: IActiveUserDailyRepository;

    @Inject(IOCServiceName.ACTIVE_USER_REALTIME_REPOSITORY)
    private readonly ActiveUserRealtimeRepository: IActiveUserRealtimeRepository;

    public async importDailyDataFromGA4ToClickHouse() {
        let model:ActiveUserDailyModel = new ActiveUserDailyModel();
        model = await this.ReportService.getActiveUserReportOfYesterday();

        if (model != null) {
            return await this.ActiveUserDailyRepository.add(model)
                .then((result) => {
                    return model;
                });
        }
        return model;
    }

    public async importRealtimeDataFromGA4ToClickHouse() {
        let result = null;
        let model:ActiveUserRealtimeModel = new ActiveUserRealtimeModel();
        model = await this.ReportService.getActiveUserRealtimeReport();

        if (model != null) {
            result = this.ActiveUserRealtimeRepository.add(model);
        }
        return result;
    }
}
export default ImportDataService;
