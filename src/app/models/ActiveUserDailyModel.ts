import { BaseModel } from './BaseModel';
import * as momentTz from "moment-timezone";
import { MOMENT_DATE_FORMAT, TIME_ZONE } from '../constants';

export class ActiveUserDailyModel extends BaseModel {
    public activeDate: momentTz.Moment;
    public active1DayUsers: number;
    public active7DayUsers: number;
    public active28DayUsers: number;

    public static fromReport(reports: any): ActiveUserDailyModel {
        let model: ActiveUserDailyModel = new ActiveUserDailyModel();
        let data: any = null;
        if (reports.rows && reports.rows.length > 0) {
            data = reports.rows[0];
            if (data.metricValues && data.metricValues.length >= 3) {
                model.active1DayUsers = data.metricValues[0].value || 0;
                model.active7DayUsers = data.metricValues[1].value || 0;
                model.active28DayUsers = data.metricValues[2].value || 0;
            }
            if (data.dimensionValues && data.dimensionValues.length != 0) {
                let day = "";
                let month = "";
                let year = "";
                if (data.dimensionValues[0].value) {
                    let dateString: string = data.dimensionValues[0].value;
                    day = dateString.substring(6, 8);
                    month = dateString.substring(4, 6);
                    year = dateString.substring(0, 4);
                    const date = momentTz.tz(`${year}-${month}-${day}`, MOMENT_DATE_FORMAT.YYYY_MM_DD, TIME_ZONE.TIME_ZONE_UTC);
                    model.activeDate = date;
                }
            }
        }
        return model;
    }
}

export default ActiveUserDailyModel;
