import { BaseModel } from './BaseModel';
import * as momentTz from "moment-timezone";
import { TIME_ZONE } from '../constants';

interface IUserPerMinuteModel {
    minuteIndex: number;
    value: number;
}
interface ITopCountryModel {
    countryName: number;
    value: number;
}

export class ActiveUserRealtimeModel extends BaseModel {
    public activeDate: momentTz.Moment;
    public totalActiveUser: number;
    public usersPerMinute: IUserPerMinuteModel[];
    public topCountries: ITopCountryModel[];

    public static fromReport(activeUserPerMinuteReport: any, activeUserByCountryReport: any, totalActiveUserReport: any): ActiveUserRealtimeModel {
        let model: ActiveUserRealtimeModel = new ActiveUserRealtimeModel();
        model.totalActiveUser = 0;
        model.topCountries = [];
        model.activeDate = momentTz.tz(new Date(), TIME_ZONE.TIME_ZONE_UTC);
        model.usersPerMinute = Array(30).fill(null).map((item, i) => {
            return {
                minuteIndex: i,
                value: 0
            };
        });

        if (activeUserPerMinuteReport.rows && activeUserPerMinuteReport.rows.length > 0) {
            activeUserPerMinuteReport.rows.forEach((item) => {
                const minuteIndex = item.dimensionValues[0].value || null;
                const value = item.metricValues[0].value || 0;
                let usersPerMinute: IUserPerMinuteModel = {
                    minuteIndex: minuteIndex,
                    value: value
                };
                model.usersPerMinute[BaseModel.getNumber(minuteIndex)] = usersPerMinute;
            });
        }
        if (activeUserByCountryReport.rows && activeUserByCountryReport.rows.length > 0) {
            activeUserByCountryReport.rows.forEach((item) => {
                const countryName = item.dimensionValues[0].value || null;
                const value = item.metricValues[0].value || 0;
                let topCountry: ITopCountryModel = {
                    countryName: countryName,
                    value: value
                };
                model.topCountries.push(topCountry);
            });
        }
        if (totalActiveUserReport.rows && totalActiveUserReport.rows.length > 0) {
            model.totalActiveUser = totalActiveUserReport.rows[0].metricValues[0].value;
        }
        return model;
    }
}

export default ActiveUserRealtimeModel;
