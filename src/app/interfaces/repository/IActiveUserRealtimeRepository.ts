import { ActiveUserRealtimeModel } from '../../models';
import { IBaseRepository } from './IBaseRepository';

export interface IActiveUserRealtimeRepository extends IBaseRepository {
    add(model: ActiveUserRealtimeModel)
}
