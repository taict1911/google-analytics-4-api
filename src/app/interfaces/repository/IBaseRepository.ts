export interface IBaseRepository {
    raw(query: string): Promise<any>;
    get(query: string): Promise<any>;
}
