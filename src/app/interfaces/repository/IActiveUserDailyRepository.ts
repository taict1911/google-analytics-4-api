import { ActiveUserDailyModel } from '../../models';
import { IBaseRepository } from './IBaseRepository';

export interface IActiveUserDailyRepository extends IBaseRepository {
    add(model: ActiveUserDailyModel)
}
