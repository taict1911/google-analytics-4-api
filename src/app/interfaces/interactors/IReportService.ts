export interface IReportService {
    getActiveUserReportOfYesterday();
    getActiveUserRealtimeReport();
}

