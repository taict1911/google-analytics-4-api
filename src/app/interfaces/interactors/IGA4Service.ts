export interface IDimensionParam {
    name: string;
}
export interface IMetricParam {
    name: string;
}
export interface IOrderByParam {
    desc: boolean,
    dimension: {
        dimensionName: string
    }
}

export interface IDateRangesParam {
    startDate: string,
    endDate: string
}

export interface IGA4Service {
    getActiveUserReport(dimensions: IDimensionParam[], metrics: IMetricParam[], dateRanges: IDateRangesParam[], orderBys: IOrderByParam[]);
    getActiveUserRealtimeReport(dimensionParams: IDimensionParam[], metricPrams: IMetricParam[], orderBys: IOrderByParam[]);
}

