export interface IImportDataService {
    importDailyDataFromGA4ToClickHouse();
    importRealtimeDataFromGA4ToClickHouse();
}

