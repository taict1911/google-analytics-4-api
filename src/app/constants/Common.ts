
export const MOMENT_DATE_FORMAT = {
    YYYY_MM_DD: "YYYY-MM-DD",
    DD_MMM_YY: "DD MMM YY",
    DD_MMM_YYYY: "DD MMM YYYY",
    DD_MMM_YY_H_m: "DD MMM YY HH:mm",
    MM_DD_YYYY: "MM-DD-YYYY",
    DD_MM_YYYY: "DD-MM-YYYY",
    YYYY_MM_DD_H_m: "YYYY-MM-DD HH:mm",
    MM_DD_YYYY_H_m: "MM-DD-YYYY HH:m",
    DD_MM_YYYY_H_m: "DD-MM-YYYY HH:mm",
    DD_MMMM_YYYY_hh_mm_A: "DD MMMM YYYY, hh:mm A",
    HH_MM: "HH:mm",
    HH_MM_A: "hh:mm a",
    SEND_MAIL_FULL_DATE: "dddd, DD MMM YYYY",
    MMM_YY: "MMM YY",
    MMM_YYYY: "MMM YYYY",
    SEND_MAIL_FULL_DATE_TIME: "hh:mm a, dddd, DD MMM YYYY",
    DD_MMM_YYYY_hh_mm_A: "DD MMM YYYY, hh:mm A",
    YYYY_MM_DD_H_m_s: "YYYY-MM-DD HH:mm:ss",
};

export const TIME_ZONE = {
    TIME_ZONE_DEFAULT: "Asia/Singapore",
    TIME_ZONE_VN: "Asia/Ho_Chi_Minh",
    TIME_ZONE_UTC: "UTC",
};

export const APP = {
    MODE: {
        DAILY: 0,
        REALTIME: 1
    }
};
