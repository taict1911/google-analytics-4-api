export const GA4_PROPERTY_NAME = {
    METRIC_VALUES: "metricValues",
    DIMENSION_VALUES: "dimensionValues",
};

export const GA4_DATE_RANGE_FORMAT = {
    YESTERDAY: "yesterday",
    TODAY: "today"
};

export const GA4_REPORT = {
    DIMENSIONS: {
        MINUTES_AGO: "minutesAgo",
        COUNTRY: "country",
        DATE: "date"
    },
    METRICS: {
        ACTIVE_USERS: "activeUsers",
        ACTIVE_1_DAY_USERS: "active1DayUsers",
        ACTIVE_7_DAY_USERS: "active7DayUsers",
        ACTIVE_28_DAY_USERS: "active28DayUsers",
    }
};
