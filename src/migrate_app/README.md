How to use DBmate for migration

1. Build and deploy docker image
    make build
2. Go to Shell (Pod Shell)
    dbmate up

For rollback 
    dbmate rollback

Link: https://github.com/amacneil/dbmate
