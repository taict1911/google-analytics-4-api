-- migrate:up

CREATE TABLE mard.active_user_daily(
    id UUID DEFAULT generateUUIDv4(),  	
    is_enable UInt8 DEFAULT 1,
    is_deleted UInt8 DEFAULT 0,
    created_date DateTime  DEFAULT now(),
    updated_date DateTime  DEFAULT now(),
    active_date DateTime NOT NULL,
    active_1_day_users UInt256  DEFAULT 0,
    active_7_day_users UInt256  DEFAULT 0,
    active_28_day_users UInt256  DEFAULT 0
)
ENGINE = MergeTree()
PRIMARY KEY (active_date)
ORDER BY (active_date);

-- migrate:down

DROP TABLE mard.active_user_daily



