
--
-- Database schema
--

CREATE DATABASE mard IF NOT EXISTS;

CREATE TABLE mard.active_user_daily
(
    `id` UUID DEFAULT generateUUIDv4(),
    `is_enable` UInt8 DEFAULT 1,
    `is_deleted` UInt8 DEFAULT 0,
    `created_date` DateTime DEFAULT now(),
    `updated_date` DateTime DEFAULT now(),
    `active_date` DateTime,
    `active_1_day_users` UInt256 DEFAULT 0,
    `active_7_day_users` UInt256 DEFAULT 0,
    `active_28_day_users` UInt256 DEFAULT 0
)
ENGINE = MergeTree
PRIMARY KEY active_date
ORDER BY active_date
SETTINGS index_granularity = 8192;

CREATE TABLE mard.active_user_realtime
(
    `id` UUID DEFAULT generateUUIDv4(),
    `is_enable` UInt8 DEFAULT 1,
    `is_deleted` UInt8 DEFAULT 0,
    `created_date` DateTime DEFAULT now(),
    `updated_date` DateTime DEFAULT now(),
    `active_date` DateTime,
    `users_per_minute` String,
    `top_countries` String,
    `total_active_user` UInt256 DEFAULT 0
)
ENGINE = MergeTree
PRIMARY KEY active_date
ORDER BY active_date
SETTINGS index_granularity = 8192;

CREATE TABLE mard.schema_migrations
(
    `version` String,
    `ts` DateTime DEFAULT now(),
    `applied` UInt8 DEFAULT 1
)
ENGINE = ReplacingMergeTree(ts)
PRIMARY KEY version
ORDER BY version
SETTINGS index_granularity = 8192;


--
-- Dbmate schema migrations
--

INSERT INTO schema_migrations (version) VALUES
    ('20220401101157'),
    ('20220401125155'),
    ('20220401125732');
