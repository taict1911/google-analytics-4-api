FROM node:14.14.0-buster-slim as builder
ENV NODE_ENV="development"

# Copy app's source code to the /app directory
COPY . /app

# The application's directory will be the working directory
WORKDIR /app

# Install Node.js dependencies defined in '/app/packages.json'
RUN npm install
RUN npm run build

FROM node:14.14.0-buster-slim
ENV NODE_ENV="production"
COPY --from=builder /app/build /app
WORKDIR /app
RUN npm install --production
CMD [ "node", "app.js" ]
