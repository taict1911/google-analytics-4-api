.PHONY: build upload

build:
	docker build -t registry.gitlab.com/stageit-app/icondo-project/taichung/ga4:latest --no-cache .

upload:
	docker push registry.gitlab.com/stageit-app/icondo-project/taichung/ga4:latest